﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Effects;
using System.IO;
using Microsoft.Win32;
using System.Reflection;
using PuzzleFarm.Properties;
using System.Windows.Threading;
using System.Media;


namespace Jigsaw
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region attributes
        List<Piece> currentSelection = new List<Piece>();
        int selectionAngle = 0;
        List<Piece> pieces = new List<Piece>();
        List<Piece> shadowPieces = new List<Piece>();
        int columns = 5;
        int rows = 4;
        double scale = 1.0;
        BitmapImage imageSource;
        string srcFileName = "";
        string destFileName;
        DropShadowBitmapEffect shadowEffect;
        Point lastCell = new Point(-1, 0);
        ScaleTransform stZoomed = new ScaleTransform() { ScaleX = 1.1, ScaleY = 1.1 };
        ViewMode currentViewMode = ViewMode.Puzzle;
        PngBitmapEncoder png;
        double offsetX = -1;
        double offsetY = -1;
        double lastMouseDownX = -1;
        double lastMouseDownY = -1;
        bool moving = false;
        double initialRectangleX = 0;
        double initialRectangleY = 0;
        Rectangle rectSelection = new Rectangle();
        #endregion attributes
        double level = 1;
        int timeSec = 0;
        int timeMin = 0;
        DispatcherTimer timerGame;
        double moves = 0;
        double points = 0;
        SoundPlayer musicGame = new SoundPlayer();
        
        

        #region constructor
        public MainWindow()
        {
            InitializeComponent();

            destFileName = Settings.Default.DestinationFile;

            cnvPuzzle.MouseLeftButtonUp += new MouseButtonEventHandler(cnvPuzzle_MouseLeftButtonUp);
            cnvPuzzle.MouseDown += new MouseButtonEventHandler(cnvPuzzle_MouseDown);
            cnvPuzzle.MouseMove += new MouseEventHandler(cnvPuzzle_MouseMove);
            cnvPuzzle.MouseWheel += new MouseWheelEventHandler(cnvPuzzle_MouseWheel);
            cnvPuzzle.MouseEnter += new MouseEventHandler(cnvPuzzle_MouseEnter);
            musicGame.SoundLocation = @"" + System.AppDomain.CurrentDomain.BaseDirectory + "\\Music\\puzzleFarm.wav";
            musicGame.PlayLooping();

            shadowEffect = new DropShadowBitmapEffect()
            {
                Color = Colors.Black,
                Direction = 320,
                ShadowDepth = 25,
                Softness = 1,
                Opacity = 0.5
            };
        }

        #endregion constructor

        #region methods

        private void CreatePuzzle(Stream streamSource)
        {
            Random rnd = new Random();
            var connections = new int[] { (int)ConnectionType.Tab, (int)ConnectionType.Blank };

            png = null;

            imageSource = null;
            var uri = new Uri(destFileName);

            using (WrappingStream wrapper = new WrappingStream(streamSource))
            using (BinaryReader reader = new BinaryReader(wrapper))
            {
                imageSource = new BitmapImage();
                imageSource.BeginInit();
                imageSource.CacheOption = BitmapCacheOption.OnLoad;
                imageSource.StreamSource = reader.BaseStream; 
                imageSource.EndInit();
                imageSource.Freeze();
            }

            imgShowImage.Source = imageSource;

            scvImage.Visibility = Visibility.Hidden;
            cnvPuzzle.Visibility = Visibility.Visible;

            var angles = new int[] { 0, 90, 180, 270 };



            int index = 0;
            for (var y = 0; y < rows; y++)
            {
                for (var x = 0; x < columns; x++)
                {
                    if (x != 1000)
                    {
                        int upperConnection = (int)ConnectionType.None;
                        int rightConnection = (int)ConnectionType.None;
                        int bottomConnection = (int)ConnectionType.None;
                        int leftConnection = (int)ConnectionType.None;

                        if (y != 0)
                            upperConnection = -1 * pieces[(y - 1) * columns + x].BottomConnection;

                        if (x != columns - 1)
                            rightConnection = connections[rnd.Next(2)];

                        if (y != rows - 1)
                            bottomConnection = connections[rnd.Next(2)];

                        if (x != 0)
                            leftConnection = -1 * pieces[y * columns + x - 1].RightConnection;

                        int angle = 0;

                        var piece = new Piece(imageSource, x, y, 0.1, 0.1, (int)upperConnection, (int)rightConnection, (int)bottomConnection, (int)leftConnection, false, index, scale);
                        piece.SetValue(Canvas.ZIndexProperty, 1000 + x * rows + y);
                        piece.MouseLeftButtonUp += new MouseButtonEventHandler(piece_MouseLeftButtonUp);
                        piece.MouseRightButtonUp += new MouseButtonEventHandler(piece_MouseRightButtonUp);
                        piece.Rotate(piece, angle);

                        var shadowPiece = new Piece(imageSource, x, y, 0.1, 0.1, (int)upperConnection, (int)rightConnection, (int)bottomConnection, (int)leftConnection, true, shadowPieces.Count(), scale);
                        shadowPiece.SetValue(Canvas.ZIndexProperty, x * rows + y);
                        shadowPiece.Rotate(piece, angle);

                        pieces.Add(piece);
                        shadowPieces.Add(shadowPiece);
                        index++;
                    }
                }
            }

            var tt = new TranslateTransform() { X = 20, Y = 20 };

            foreach (var p in pieces)
            {
                Random random = new Random();
                int i = random.Next(0, pnlPickUp.Children.Count);

                p.ScaleTransform.ScaleX = 1.0;
                p.ScaleTransform.ScaleY = 1.0;
                p.RenderTransform = tt;
                p.X = -1;
                p.Y = -1;
                p.IsSelected = false;

                pnlPickUp.Children.Insert(i, p);

                double angle = angles[rnd.Next(0, 4)];
                p.Rotate(p, angle);
                shadowPieces[p.Index].Rotate(p, angle);
            }


            rectSelection.SetValue(Canvas.ZIndexProperty, 5000);

            rectSelection.StrokeDashArray = new DoubleCollection(new double[] {4,4,4,4});
            cnvPuzzle.Children.Add(rectSelection);
        }

        private void DestroyReferences()
        {
            for (var i = cnvPuzzle.Children.Count - 1; i >= 0; i--)
            {
                if (cnvPuzzle.Children[i] is Piece)
                {
                    Piece p = (Piece)cnvPuzzle.Children[i];
                    p.MouseLeftButtonUp -= new MouseButtonEventHandler(piece_MouseLeftButtonUp);
                    p.ClearImage();
                    cnvPuzzle.Children.Remove(p);
                }
            }

            cnvPuzzle.Children.Clear();
            SetSelectionRectangle(-1, -1, -1, -1);

            for (var i = pnlPickUp.Children.Count - 1; i >= 0; i--)
            {
                Piece p = (Piece)pnlPickUp.Children[i];
                p.ClearImage();
                p.MouseLeftButtonUp -= new MouseButtonEventHandler(piece_MouseLeftButtonUp);
                pnlPickUp.Children.Remove(p);
            }

            pnlPickUp.Children.Clear();

            for (var i = pieces.Count - 1; i >= 0; i--)
            {
                pieces[i].ClearImage();
            }

            for (var i = shadowPieces.Count - 1; i >= 0; i--)
            {
                shadowPieces[i].ClearImage();
            }

            shadowPieces.Clear();
            pieces.Clear();
            imgShowImage.Source = null;
            imageSource = null;
        }

        private Stream LoadImage(string srcFileName)
        {
            imageSource = new BitmapImage(new Uri(srcFileName));
            columns = (int)Math.Ceiling(imageSource.PixelWidth / 100.0);
            rows = (int)Math.Ceiling(imageSource.PixelHeight / 100.0);

            var bi = new BitmapImage(new Uri(srcFileName));
            var imgBrush = new ImageBrush(bi);
            imgBrush.AlignmentX = AlignmentX.Left;
            imgBrush.AlignmentY = AlignmentY.Top;
            imgBrush.Stretch = Stretch.UniformToFill;

            RenderTargetBitmap rtb = new RenderTargetBitmap((columns + 1) * 100, (rows + 1) * 100, bi.DpiX, bi.DpiY, PixelFormats.Pbgra32);
            var rectImage = new Rectangle();
            rectImage.Width = imageSource.PixelWidth;
            rectImage.Height = imageSource.PixelHeight;
            rectImage.HorizontalAlignment = HorizontalAlignment.Left;
            rectImage.VerticalAlignment = VerticalAlignment.Top;
            rectImage.Fill = imgBrush;
            rectImage.Arrange(new Rect((columns * 100 - imageSource.PixelWidth) / 2, (rows * 100 - imageSource.PixelHeight) / 2, imageSource.PixelWidth, imageSource.PixelHeight));

            rectImage.Margin = new Thickness(
                (columns * 100 - imageSource.PixelWidth) / 2,
                (rows * 100 - imageSource.PixelHeight) / 2,
                (rows * 100 - imageSource.PixelHeight) / 2,
                (columns * 100 - imageSource.PixelWidth) / 2);
            rtb.Render(rectImage);

            png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(rtb));

            Stream ret = new MemoryStream();

            png.Save(ret);

            return ret;
        }

        private bool IsPuzzleCompleted()
        {
            var query = from p in pieces
                        where p.Angle != 0
                        select p;

            if (query.Any())
                return false;

            query = from p1 in pieces
                    join p2 in pieces on p1.Index equals p2.Index - 1
                    where (p1.Index % columns < columns - 1) && (p1.X + 1 != p2.X)
                    select p1;

            if (query.Any())
                return false;

            query = from p1 in pieces
                    join p2 in pieces on p1.Index equals p2.Index - columns
                    where (p1.Y + 1 != p2.Y)
                    select p1;

            if (query.Any())
                return false;

            return true;
        }

        private void ResetZIndexes()
        {
            int zIndex = 0;
            foreach (var p in shadowPieces)
            {
                p.SetValue(Canvas.ZIndexProperty, zIndex);
                zIndex++;
            }
            foreach (var p in pieces)
            {
                p.SetValue(Canvas.ZIndexProperty, zIndex);
                zIndex++;
            }
        }

        private bool TrySetCurrentPiecePosition(double newX, double newY)
        {
            bool ret = true;

            double cellX = (int)((newX) / 100);
            double cellY = (int)((newY) / 100);

            var firstPiece = currentSelection[0];

            foreach (var currentPiece in currentSelection)
            {
                var relativeCellX = currentPiece.X - firstPiece.X;
                var relativeCellY = currentPiece.Y - firstPiece.Y;

                double rotatedCellX = 0;
                double rotatedCellY = 0;
                rotatedCellX = relativeCellX;
                rotatedCellY = relativeCellY;

                var q = from p in pieces
                        where (
                                (p.Index != currentPiece.Index) &&
                                (!p.IsSelected) &&
                                (cellX + rotatedCellX > 0) &&
                                (cellY + rotatedCellY > 0) &&
                                (
                                ((p.X == cellX + rotatedCellX) && (p.Y == cellY + rotatedCellY))
                                || ((p.X == cellX + rotatedCellX - 1) && (p.Y == cellY + rotatedCellY) && 
                                (p.RightConnection + currentPiece.LeftConnection != 0))
                                || ((p.X == cellX + rotatedCellX + 1) && (p.Y == cellY + rotatedCellY) && 
                                (p.LeftConnection + currentPiece.RightConnection != 0))
                                || ((p.X == cellX + rotatedCellX) && (p.Y == cellY - 1 + rotatedCellY) && 
                                (p.BottomConnection + currentPiece.UpperConnection != 0))
                                || ((p.X == cellX + rotatedCellX) && (p.Y == cellY + 1 + rotatedCellY) && 
                                (p.UpperConnection + currentPiece.BottomConnection != 0))
                                )
                              )
                        select p;

                if (q.Any())
                {
                    ret = false;
                    break;
                }
            }

            return ret;
        }

        private Point SetCurrentPiecePosition(Piece currentPiece, double newX, double newY)
        {
            double cellX = (int)((newX) / 100);
            double cellY = (int)((newY) / 100);

            var firstPiece = currentSelection[0];

            var relativeCellX = currentPiece.X - firstPiece.X;
            var relativeCellY = currentPiece.Y - firstPiece.Y;

            double rotatedCellX = relativeCellX;
            double rotatedCellY = relativeCellY;

            currentPiece.X = cellX + rotatedCellX;
            currentPiece.Y = cellY + rotatedCellY;

            currentPiece.SetValue(Canvas.LeftProperty, currentPiece.X * 100);
            currentPiece.SetValue(Canvas.TopProperty, currentPiece.Y * 100);

            shadowPieces[currentPiece.Index].SetValue(Canvas.LeftProperty, currentPiece.X * 100);
            shadowPieces[currentPiece.Index].SetValue(Canvas.TopProperty, currentPiece.Y * 100);

            return new Point(cellX, cellY);
        }

        private void SetSelectionRectangle(double x1, double y1, double x2, double y2)
        {
            
            double x = (x2 >= x1) ? x1 : x2;
            double y = (y2 >= y1) ? y1 : y2;
            double width = Math.Abs(x2 - x1);
            double height = Math.Abs(y2 - y1);
            rectSelection.Visibility = System.Windows.Visibility.Visible;
            rectSelection.Width = width;
            rectSelection.Height = height;
            rectSelection.Stroke = new SolidColorBrush(Colors.Red);

            rectSelection.SetValue(Canvas.LeftProperty, x);
            rectSelection.SetValue(Canvas.TopProperty, y);
        }

        private void calculatePoints()
        {

            points = (points + (level * 1000)) - (moves + (timeMin * 60) + timeSec);
            lblPuntuacion.Content = points;

        }

        private void MouseUp()
        {

            if (currentSelection.Count == 0)
            {
                double x1 = (double)rectSelection.GetValue(Canvas.LeftProperty) - 20;
                double y1 = (double)rectSelection.GetValue(Canvas.TopProperty) - 20;
                double x2 = x1 + rectSelection.Width;
                double y2 = y1 + rectSelection.Height;

                int cellX1 = (int)(x1 / 100);
                int cellY1 = (int)(y1 / 100);
                int cellX2 = (int)(x2 / 100);
                int cellY2 = (int)(y2 / 100);

                var query = from p in pieces
                            where
                            (p.X >= cellX1) && (p.X <= cellX2) &&
                            (p.Y >= cellY1) && (p.Y <= cellY2)
                            select p;

                foreach (var currentPiece in query)
                {
                    currentSelection.Add(currentPiece);

                    currentPiece.SetValue(Canvas.ZIndexProperty, 5000);
                    shadowPieces[currentPiece.Index].SetValue(Canvas.ZIndexProperty, 4999);
                    currentPiece.BitmapEffect = shadowEffect;

                    currentPiece.RenderTransform = stZoomed;
                    currentPiece.IsSelected = true;
                    shadowPieces[currentPiece.Index].RenderTransform = stZoomed;
                }
                SetSelectionRectangle(-1, -1, -1, -1);
            }
            else
            {
                moves++;
                var newX = Mouse.GetPosition(cnvPuzzle).X - 20;
                var newY = Mouse.GetPosition(cnvPuzzle).Y - 20;
                if (TrySetCurrentPiecePosition(newX, newY))
                {
                    int count = currentSelection.Count;
                    for (int i = count - 1; i >= 0; i--)
                    {
                        var currentPiece = currentSelection[i];

                        currentPiece.BitmapEffect = null;
                        ScaleTransform st = new ScaleTransform() { ScaleX = 1.0, ScaleY = 1.0 };
                        currentPiece.RenderTransform = st;
                        currentPiece.IsSelected = false;
                        shadowPieces[currentPiece.Index].RenderTransform = st;
                        
                        lastCell = SetCurrentPiecePosition(currentPiece, newX, newY);

                        ResetZIndexes();

                        currentPiece = null;
                    }

                    currentSelection.Clear();

                    if (IsPuzzleCompleted())
                    {
                        calculatePoints();
                        level++;
                        lblLevel.Content = level;
                        timerGame.Stop();
                        if (level < 6)
                        {
                            var result = MessageBox.Show("¡Felicidades!\r\n Has pasado de nivel.", "¡Nivel Completado!", MessageBoxButton.OK, MessageBoxImage.Information);
                            newPuzzle();
                        }
                        else
                        {

                            var result = MessageBox.Show("¡Felicidades!\r\n Has ganado.", "¡Juego Completado!", MessageBoxButton.OK, MessageBoxImage.Information);
                            grdBackGame.Visibility = Visibility.Visible;
                        }
                    }
                }
                selectionAngle = 0;
            }
        }

        #endregion methods

        #region events

        void piece_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (currentSelection.Count > 0)
            {
                var axisPiece = currentSelection[0];
                foreach (var currentPiece in currentSelection)
                {
                    double deltaX = axisPiece.X - currentPiece.X;
                    double deltaY = axisPiece.Y - currentPiece.Y;

                    double targetCellX = deltaY;
                    double targetCellY = -deltaX;

                    currentPiece.Rotate(axisPiece, 90);
                    shadowPieces[currentPiece.Index].Rotate(axisPiece, 90);
                }
                selectionAngle += 90;
                if (selectionAngle == 360)
                    selectionAngle = 0;
            }
        }

        void piece_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            var chosenPiece = (Piece)sender;

            if (chosenPiece.Parent is WrapPanel)
            {
                if (currentSelection.Count() > 0)
                {
                    var p = currentSelection[0];
                    cnvPuzzle.Children.Remove(p);
                    cnvPuzzle.Children.Remove(shadowPieces[p.Index]);

                    var tt = new TranslateTransform() { X = 20, Y = 20 };
                    p.ScaleTransform.ScaleX = 1.0;
                    p.ScaleTransform.ScaleY = 1.0;
                    p.RenderTransform = tt;
                    p.X = -1;
                    p.Y = -1;
                    p.IsSelected = false;
                    p.SetValue(Canvas.ZIndexProperty, 0);
                    p.BitmapEffect = null;
                    p.Visibility = System.Windows.Visibility.Visible;
                    pnlPickUp.Children.Add(p);

                    currentSelection.Clear();
                }
                else
                {
                    pnlPickUp.Children.Remove(chosenPiece);
                    cnvPuzzle.Children.Add(shadowPieces[chosenPiece.Index]);
                    chosenPiece.SetValue(Canvas.ZIndexProperty, 5000);
                    shadowPieces[chosenPiece.Index].SetValue(Canvas.ZIndexProperty, 4999);
                    chosenPiece.BitmapEffect = shadowEffect;
                    chosenPiece.RenderTransform = stZoomed;
                    shadowPieces[chosenPiece.Index].RenderTransform = stZoomed;
                    cnvPuzzle.Children.Add(chosenPiece);
                    chosenPiece.Visibility = Visibility.Hidden;
                    shadowPieces[chosenPiece.Index].Visibility = Visibility.Hidden;
                    chosenPiece.IsSelected = true;
                    currentSelection.Add(chosenPiece);
                }
            }
        }

        void cnvPuzzle_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            
            MouseUp();
            lblMove.Content = moves;
        }

        void cnvPuzzle_MouseEnter(object sender, MouseEventArgs e)
        {
            if (currentSelection.Count > 0)
            {
                foreach (var currentPiece in currentSelection)
                {
                    currentPiece.Visibility = Visibility.Visible;
                    if (shadowPieces.Count > currentPiece.Index)
                        shadowPieces[currentPiece.Index].Visibility = Visibility.Visible;
                }
            }
        }

        void cnvPuzzle_MouseWheel(object sender, MouseWheelEventArgs e)
        {

        }

        void cnvPuzzle_MouseDown(object sender, MouseButtonEventArgs e)
        {
            

            initialRectangleX = Mouse.GetPosition((IInputElement)sender).X;
            initialRectangleY = Mouse.GetPosition((IInputElement)sender).Y;
            SetSelectionRectangle(initialRectangleX, initialRectangleY, initialRectangleX, initialRectangleY);

        }

        void cnvPuzzle_MouseMove(object sender, MouseEventArgs e)
        {
            MouseMoving();
        }

        private void MouseMoving()
        {
            var newX = Mouse.GetPosition((IInputElement)cnvPuzzle).X - 20;
            var newY = Mouse.GetPosition((IInputElement)cnvPuzzle).Y - 20;

            int cellX = (int)((newX) / 100);
            int cellY = (int)((newY) / 100);

            if (Mouse.LeftButton == MouseButtonState.Pressed)
            {
                SetSelectionRectangle(initialRectangleX, initialRectangleY, newX, newY);
            }
            else
            {
                if (currentSelection.Count > 0)
                {
                    var firstPiece = currentSelection[0];

                    foreach (var currentPiece in currentSelection)
                    {
                        var relativeCellX = currentPiece.X - firstPiece.X;
                        var relativeCellY = currentPiece.Y - firstPiece.Y;

                        double rotatedCellX = relativeCellX;
                        double rotatedCellY = relativeCellY;

                        currentPiece.SetValue(Canvas.LeftProperty, newX - 50 + rotatedCellX * 100);
                        currentPiece.SetValue(Canvas.TopProperty, newY - 50 + rotatedCellY * 100);

                        shadowPieces[currentPiece.Index].SetValue(Canvas.LeftProperty, newX - 50 + rotatedCellX * 100);
                        shadowPieces[currentPiece.Index].SetValue(Canvas.TopProperty, newY - 50 + rotatedCellY * 100);
                    }
                }
            }
        }

        void cnvPuzzle_MouseLeave(object sender, MouseEventArgs e)
        {
            SetSelectionRectangle(-1, -1, -1, -1);
            if (currentSelection.Count() > 0)
            {
                int count = currentSelection.Count();
                for (var i = count - 1; i >= 0; i--)
                {
                    var p = currentSelection[i];
                    cnvPuzzle.Children.Remove(p);
                    cnvPuzzle.Children.Remove(shadowPieces[p.Index]);

                    var tt = new TranslateTransform() { X = 20, Y = 20 };
                    p.ScaleTransform.ScaleX = 1.0;
                    p.ScaleTransform.ScaleY = 1.0;
                    p.RenderTransform = tt;
                    p.X = -1;
                    p.Y = -1;
                    p.IsSelected = false;
                    p.SetValue(Canvas.ZIndexProperty, 0);
                    p.BitmapEffect = null;
                    p.Visibility = System.Windows.Visibility.Visible;
                    pnlPickUp.Children.Add(p);
                }
                currentSelection.Clear();
            }
            MouseUp();
        }

        private void newPuzzle()
        {
            timeSec = 0;
            timeMin = 0;
            moves = 0;
            timerGame = new DispatcherTimer();
            timerGame.Interval = TimeSpan.FromMilliseconds(1000.0);
            timerGame.Tick += new EventHandler(crono);
            timerGame.Start();
            try
            {
                DestroyReferences();
                srcFileName = System.IO.Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory + "\\Puzzles\\" + level + ".png");
                using (Stream streamSource = LoadImage(srcFileName))
                {
                    CreatePuzzle(streamSource);
                }
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.ToString());
            }


        }
        private void btnNewPuzzle_Click(object sender, MouseButtonEventArgs e)
        {
            level = 1;
            moves = 0;
            points = 0;
            lblLevel.Content = level;
            lblMove.Content = moves;
            lblPuntuacion.Content = points;
            newPuzzle();
            grdMain.Visibility = Visibility.Hidden;
            grdBackGame.Visibility = Visibility.Hidden;
           
        }
        private void crono(object sender, EventArgs eArgs)
        {
            
            timeSec++;
            if (timeSec == 60)
            {
                timeSec = 0;
                timeMin++;
            }
            if (timeSec < 10)
            {
                lblTimer.Content = timeMin + ":0" + timeSec;
            }
            else
            {
                lblTimer.Content = timeMin + ":" + timeSec;
            }
        }
        private void grdTop_MouseEnter(object sender, MouseEventArgs e)
        {
            grdWindow.RowDefinitions[0].Height = new GridLength(0);
        }

        private void StackPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            grdWindow.RowDefinitions[0].Height = new GridLength(32);
        }

        private void DockPanel_MouseEnter(object sender, MouseEventArgs e)
        {
            grdWindow.RowDefinitions[0].Height = new GridLength(32);
        }

        private void brdWindow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            moving = !moving;
            if (moving)
            {
                offsetX = Mouse.GetPosition((IInputElement)sender).X - (double)this.GetValue(Canvas.LeftProperty);
                offsetY = Mouse.GetPosition((IInputElement)sender).Y - (double)this.GetValue(Canvas.TopProperty);
                lastMouseDownX = Mouse.GetPosition((IInputElement)sender).X;
                lastMouseDownY = Mouse.GetPosition((IInputElement)sender).Y;
               
            }
        }

        #endregion events

        private void helpOff(object sender, MouseButtonEventArgs e)
        {
            lblHelpOn.Visibility = Visibility.Visible;
            lblHelpOff.Visibility = Visibility.Hidden;
            grdPuzzle.Visibility = Visibility.Visible;
            scvImage.Visibility = Visibility.Hidden;
            currentViewMode = ViewMode.Puzzle;

        }

        private void helpOn(object sender, MouseButtonEventArgs e)
        {
            lblHelpOn.Visibility = Visibility.Hidden;
            lblHelpOff.Visibility = Visibility.Visible;
            grdPuzzle.Visibility = Visibility.Hidden;
            scvImage.Visibility = Visibility.Visible;
            currentViewMode = ViewMode.Picture;
        }

        private void musicOff(object sender, MouseButtonEventArgs e)
        {
            lblMusicOff.Visibility = Visibility.Hidden;
            lblMusicOn.Visibility = Visibility.Visible;
            musicGame.Stop();
        }

        private void musicOn(object sender, MouseButtonEventArgs e)
        {
            lblMusicOn.Visibility = Visibility.Hidden;
            lblMusicOff.Visibility = Visibility.Visible;
            musicGame.PlayLooping();
        }

    }

    public enum ViewMode
    {
        Picture,
        Puzzle
    }
}
